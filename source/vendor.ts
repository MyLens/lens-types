
/**
 * A vendor verification
 */
export default interface Vendor {
    /**
     * The vendor's unique Id.
     */
    id: string, 
    /**
     * The vendor's human readable name
     */
    name: string, 
    /**
     * The public key the vendor used to sign this information.
     */
    publicKey: string, 
    /**
     * The signature that was created for this information.
     */
    signature: string
}