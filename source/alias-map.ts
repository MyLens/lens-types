/**
 * Data structure that defines name mappings for data in lenses.
 */
export default interface AliasMap {
    /**
     * Each dataId with an alias (or if it is a composite that has
     * children with aliases) will have an entry in the map.
     * Each entry can have one or more aliases associated with it,
     * depending on if it is used more than once.
     */
    [dataId: string]: Array<{
        /**
         * An alias this dataId is known by.
         */
        alias?: string,
        /**
         * If this data is a composite type, it has a sub map for any children
         * that may require alias mapping. 
         */
        children?: AliasMap
    }>
}