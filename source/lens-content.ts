import LensDataIndex from './lens-data-index';
import AliasMap from './alias-map';
/**
 * Unencrypted content of a lens's content property.
 */
export default interface LensContent {
    /**
     * The array of top-level data in this lens.
     */
    data: Array<LensDataIndex>,
    /**
     */
    aliases: AliasMap,
} 