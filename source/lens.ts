/**
 * The raw data structure of a lens.
 */
export default interface Lens {
    /**
     * The id of the lens. This should be a UUID that is at least 
     * universal to the owner.
     */
    id: null | string,
    /**
     * The userId of the owner. In most circumstances this is the 
     * owner's blockstack id.
     */
    userId: null | string,
    /**
     * The public key of the owner. Content encrypted using this 
     * public key will only be retreivable by the owner.
     */
    publicKey: null | string,
    /**
     * The datetime when the lens was originally created.
     */
    created: null | number;
    /**
     * The datetime when the lens was last written.
     */
    updated: null | number;
    /**
    * The encrypted content of this lens. Only the receiver of the 
    * lens, for whom it was encrypted for, will be able to decrypt it. 
    */ 
    content: null | string,
} 