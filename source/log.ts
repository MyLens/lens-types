/**
 * Standard Unix Syslog Severity Levels
 */
export enum LogSeverityType {
    /**
     * System is unusable
     */
    Emergency = 'EMERG',
    /**
     * Action must be taken immediately
     */
    Alert = 'ALERT',
    /**
     * Crticial conditions, hard device errors
     */
    Critical = 'CRIT',
    /**
     * Error conditions
     */
    Error = 'ERR',
    /**
     * Warning conditions
     */
    Warning = 'WARNING',
    /**
     * Normal but significant conditions, Conditions that are not error conditions, but that may require special handling.
     */
    Notice = 'NOTICE',
    /**
     * Informational  messages
     */
    Informational = 'INFO',
    /**
     * Debug-level messages, Messages that contain information normally of use only when debugging a program.
     */
    Debug = 'DEBUG'
}

/**
 * The row of information in the overall log for a user.
 */
export default interface LogItem {
    /**
     * The ID of the log entry.  This is a UUID.
     */
    id: string,
    /**
     * The enumerated type of action taken.
     * E.g. 'SHARE', 'DELETE', 'REVOKE'
     */
    action: string,
    /**
     * The severity of the log item
     */
    severity: LogSeverityType,
    /**
     * UTC ISO 8601 timestamp when the action was performed.
     */
    timestamp: string,
    /**
     * Geographic location where action nwas performed.
     */
    coords?: {
        lat: number,
        long: number
    },
    /**
     * Detailed Log message about action performed
     */
    message: string
}