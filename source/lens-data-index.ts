/**
 * A reference to a data item, contained inside a Lens.
 */
export default interface LensDataIndex {
    /**
     * The id of the data item
     */
    id: string, 
    /**
     * The symmetric key use to encrypt the data
     */
    key: string, 
    /**
     * The file the data item is located in.
     */
    fileName: string, 
    /**
     * The alias (if any) the data item should be name, instead
     * of the default 'name' field.
     */
    alias?: string
}